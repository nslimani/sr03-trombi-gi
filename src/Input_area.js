import React from "react";
import logo from "./photos/logo_utc.png";
import Table_title from "./Table_title";
import './App.css';

class Input_area extends React.Component{
    constructor(props) {
        super(props);
    }

    render(){
        return(
            <div className={"page-contents"}>
                <div className="position-relative">
                    <div className="position-absolute top-0 start-50 translate-middle">
                        < img src={logo} alt="logo"/>
                    </div>

                </div>

                <div>
                    <p>Prenom</p>
                    <input type="text"  placeholder={"Rechercher"}
                           onChange={this.handelChange.bind(this)}
                           defaultValue={this.state.inpValu}/>


                    <p>Nom</p>
                    <input type="text"  placeholder={"Rechercher"}
                           onChange={this.handelChange1.bind(this)}
                           defaultValue={this.state.inpValu1}/>
                    <p>Choisir le laboratoire</p>
                    <select name ={"find_by_lab"} id={"find_by_lab"}  onChange={this.handelChange2.bind(this)} defaultValue={this.state.lab}>
                        <option value = {"TOUS"}>Tous</option>
                        <option value = {"heuristique et diagnostic des systèmes complexes"}>Heuristique et diagnostic des systèmes complexes</option>
                        <option value = {"laboratoire de mathématiques appliquées de Compiègne"}>Laboratoire de mathématiques appliquées de Compiègne</option>
                        <option value = {"connaissance, organisation et systèmes techniques"}>Connaissance, organisation et systèmes techniques</option>
                        <option value={"null"}> Pas de lab</option>
                    </select>
                    <p>Choisir les fonctions</p>

                    <select name={"find_by_function"} id={"find_by_function"} onChange={this.handelChange3.bind(this)} defaultValue={this.state.func}>
                        <option value={"TOUS"}>Tous</option>
                        <option value={"Enseignant"}>Enseignant</option>
                        <option value={"Chercheur"}>Chercheur</option>
                        <option value={"Doctorant"}>Doctorant</option>
                        <option value={"null"}>Pas de fonction</option>
                    </select>
                </div>

                {/*afficher la table*/}
                <Table_title list={list}/>

            </div>
        )
    }

}