import React from 'react';
import Codemail from './Codemail';
import Table_title from "./Table_title";
import './App.css';
import logo from './photos/logo_utc.png';
import Line_of_table from "./Line_of_table";

class App extends React.Component {
    constructor(props) {
        super(props);
        //state: 异步信息存储器
        this.state = {
            error: null,
            isLoaded: false,
            uvs: [],
            inpValu: null,
            inpValu1: null,
            lab:"TOUS",
            func: "TOUS",
            uvs_full:[]
        };
    }

    handelChange(e){
        this.setState({
            inpValu:e.target.value
        })
    }

    handelChange1(e){
        this.setState({
            inpValu1:e.target.value
        })
    }

    handelChange2(e){
        this.setState({
            lab:e.target.value
        })
    }

    handelChange3(e){
        this.setState({
            func:e.target.value
        })
    }


    componentDidUpdate(prevProps, prevState, snapshot) {
        const func = this.state.func
        const lab = this.state.lab


        if (this.state.lab !== prevState.lab || this.state.func != prevState.func) {
            let uvs_inter = []
            let uvs_func = []
         //   if (this.state.lab !== prevState.lab) {
                if (lab === "TOUS") {
                    //deep copy the array of uvs_full to uvs_inter
                    uvs_inter = this.state.uvs_full.concat()
                    console.log("uvs_inter: ", uvs_inter)
                } else {
                    for (let i = 0; i < this.state.uvs_full.length; i++) {
                        if (this.state.uvs_full[i].structLibelleFils === lab ||
                            (lab === "null" && this.state.uvs_full[i].structLibelleFils == null)) {

                            uvs_inter.push(this.state.uvs_full[i])
                        }
                    }

                }
          //  } else uvs_inter = prevState.uvs

                if (func !== "TOUS") {
                    for (let i = 0; i < uvs_inter.length; i++) {
                        if ((uvs_inter[i].fonction === null && func === "null")){
                            uvs_func.push(uvs_inter[i])
                            console.log("null: ",uvs_inter[i].fonction)
                        }
                        else if (uvs_inter[i].fonction !== null && uvs_inter[i].fonction.indexOf(func) !== -1) {
                            uvs_func.push(uvs_inter[i])
                        } else if (uvs_inter[i].fonction !== null && (func === "Enseignant" || func === "Chercheur")
                            && uvs_inter[i].fonction.slice(0, 2) === "EC") {
                            uvs_func.push(uvs_inter[i])
                        } else if (uvs_inter[i].fonction !== null && (func === "Heudiasyc" || func === "LMAC")
                            && uvs_inter[i].fonction.slice(0, 9) === "Directeur") {
                        uvs_func.push(uvs_inter[i])
                        } else if (uvs_inter[i].fonction !== null && (uvs_inter[i].fonction.slice(0, 40) === "Responsable"
                            || uvs_inter[i].fonction.slice(0, 11) === "Gestionnaire")) {
                        uvs_func.push(uvs_inter[i])
                        }

                    }
                    uvs_inter = uvs_func.concat()
                }


            this.setState({
                uvs: uvs_inter
            })
        }
    }



    componentDidMount() {
    //'https://webservices.utc.fr/api/v1/trombi/gi?photo=N'
        fetch('https://webservices.utc.fr/api/v1/trombi/gi',{
            method: 'get',
            headers: new Headers({
                'Authorization': 'Basic '+btoa('wsuser:v3Kenobi!'),
                'Content-Type': 'application/x-www-form-urlencoded'
            })
        })
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        uvs: result,
                        uvs_full: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                    console.log(error);
                }
            )
    }

    render() {
        let list = null;
        const { error, isLoaded, uvs, inpValu,inpValu1} = this.state;
        //console.log(lab,func);
        if (error) {

            return <div>Erreur : {error.message}</div>;
        } else if (!isLoaded) {

            return <div>Chargement…</div>;

        } else {
            if( (inpValu==null || inpValu==="") && (inpValu1==null || inpValu1==="")){
                //cas 1: nom et prenom sont null
                    list = uvs.map(function(item) {
                    return(
                        <Line_of_table
                           login={item.login}
                           photo={item.photo}
                           nomAz={item.nomAz}
                           prenomAz={item.prenomAz}
                           mail={item.mail}
                           structAbrFils={item.structAbrFils}
                           telPoste1 = {item.telPoste1}
                        />

                    );


                });

            }else if((inpValu!=null && inpValu!=="") && (inpValu1==null || inpValu1==="")){
                //cas 2 nom non null, prenom null
                    list = uvs.map(function(item) {

                    if( item.nomAz.indexOf(inpValu)!==-1){
                        return(
                            <Line_of_table
                               login={item.login}
                               photo={item.photo}
                               nomAz={item.nomAz}
                               prenomAz={item.prenomAz}
                               mail={item.mail}
                               structAbrFils={item.structAbrFils}
                               telPoste1 = {item.telPoste1}
                            />
                        );
                    }

                });



            }else if((inpValu===null || inpValu==="") && (inpValu1!==null && inpValu1!=="")){
                //cas 3 : nom est null, prenom non null
                    list = uvs.map(function(item) {
                    if(item.prenomAz.indexOf(inpValu1)!==-1){
                        return(
                            <Line_of_table
                               login={item.login}
                               photo={item.photo}
                               nomAz={item.nomAz}
                               prenomAz={item.prenomAz}
                               mail={item.mail}
                               structAbrFils={item.structAbrFils}
                               telPoste1 = {item.telPoste1}
                            />
                        );
                    }

                });
            }else if((inpValu!==null && inpValu!=="") && (inpValu1!==null && inpValu1!=="")){
                //cas 4 both are no null
                    list = uvs.map(function(item) {
                    if(item.prenomAz.indexOf(inpValu1)!==-1 && item.nomAz.indexOf(inpValu)!==-1 ){
                        return(
                            <Line_of_table
                                login={item.login}
                                photo={item.photo}
                                nomAz={item.nomAz}
                                prenomAz={item.prenomAz}
                                mail={item.mail}
                                structAbrFils={item.structAbrFils}
                                telPoste1 = {item.telPoste1}
                            />
                        );
                    }

                });

            }

            return(
                <div>
                    <ul>
                        <li className="image"><a href="#home">< img src={logo} alt="logo" width="100" height="50"/></a></li>
                        <li className="dropdown">
                            <a href="javascript:void(0)" className="dropbtn">Filtre</a>
                            <div className="dropdown-content">
                                <a href="#">
                                    <input type="text"  placeholder={"Nom"}
                                           onChange={this.handelChange.bind(this)}
                                           defaultValue={this.state.inpValu}/>
                                </a>
                                <a href="#">
                                    <input type="text"  placeholder={"Prenom"}
                                           onChange={this.handelChange1.bind(this)}
                                           defaultValue={this.state.inpValu1}/>
                                </a>
                                <a href="#">
                                    <select name ={"find_by_lab"} id={"find_by_lab"}
                                            onChange={this.handelChange2.bind(this)} defaultValue={this.state.lab}>
                                    <option value = {"TOUS"}>Tous les Laboratoires</option>
                                    <option value = {"heuristique et diagnostic des systèmes complexes"}>
                                        Heuristique et diagnostic des systèmes complexes</option>
                                    <option value = {"laboratoire de mathématiques appliquées de Compiègne"}>
                                        Laboratoire de mathématiques appliquées de Compiègne</option>
                                    <option value = {"connaissance, organisation et systèmes techniques"}>
                                        Connaissance, organisation et systèmes techniques</option>
                                    <option value={"null"}> Pas de lab</option>
                                </select></a>
                                <a href='#'><select name={"find_by_function"} id={"find_by_function"} onChange={this.handelChange3.bind(this)} defaultValue={this.state.func}>
                                    <option value={"TOUS"}>Tout le personnel</option>
                                    <option value={"Enseignant"}>Enseignant</option>
                                    <option value={"Chercheur"}>Chercheur</option>
                                    <option value={"Doctorant"}>Doctorant</option>
                                    <option value={"Directeur"}>Directeur de Laboratoire</option>
                                    <option value={"Responsable"}>Responsable</option>
                                    <option value={"Gestionnaire"}>Gestionnaire</option>
                                    <option value={"null"}>Pas de fonction</option>
                                </select></a>
                            </div>
                        </li>
                    </ul>
                    <Table_title list={list}/>
                </div>




            );

        }

    }
}
export default App;
