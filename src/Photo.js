import React from 'react';
import './App.css';


class Photo extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        // console.log(this.props.photo)
        return (

            < img className={"img-logo"} src={`data:image/jpg;base64,${this.props.photo}`} />

        );
    }
}


export default Photo;