import React from 'react';
import  './App.css';

class Table_title extends React.Component{

    constructor(props) {
        super(props);
    }

    render() {
        return(
            <table id={'table'} border={"1"} cellSpacing={"0"}>
                <h1 className={"table-title"} > Liste du Personnel </h1>
                <tr>
                    <th> Photo</th>
                    <th> Nom</th>
                    <th> Prenom</th>
                    <th> Email</th>
                    <th> Bureau</th>
                    <th> QrCode mail</th>
                    <th> QrCode Post</th>
                </tr>
                <tbody>
                {this.props.list}
                </tbody>
            </table>
        )
    }

}
export default Table_title