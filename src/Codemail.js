import React from 'react';
import QRCode  from 'qrcode.react'
import './App.css';

class Codemail extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        if(this.props.content!==null){
            //`post is : ${this.props.content}`
            return (
                <QRCode
                    value={this.props.content}  //value参数为生成二维码的链接
                    size={100} //二维码的宽高尺寸
                    fgColor="#000000"  //二维码的颜色
                />
            );
        }else return(
            <p></p>
        )

    }
}

export default Codemail;


