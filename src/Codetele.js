import React from 'react';
import QRCode  from 'qrcode.react'
import './App.css';

class Codetele extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        if(this.props.content!==null){

            return (
                <QRCode
                    value={`post is : ${this.props.content}`}  //value参数为生成二维码的链接
                    size={100} //二维码的宽高尺寸
                    fgColor="#000000"  //二维码的颜色
                />
            );
        }else return(
            <p></p>
        )

    }
}

export default Codetele;


